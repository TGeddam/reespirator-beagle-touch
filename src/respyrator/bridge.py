##############################################################################
# For copyright and license notices, see LICENSE file in root directory
##############################################################################
import sys
import socket
import time, threading

import numpy as np
from respyrator import core, serial


class MainBridge():

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._serial = None
        self.serial_setup()
        
        self._s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        core.logger.debug('Trying to connect with %s %d' % (core.config['host'] , core.config['remotePort']))
        self._s.connect((core.config['host'], core.config['remotePort']))

        #self._serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #self._serversocket.bind((socket.gethostname(), 1234))
        #self._serversocket.listen(2)

        threading.Timer(1, self.serial_read ).start()
        core.logger.debug('Timer configured')


    def serial_setup(self):
        file = core.config['serial_file']
        if file:
            self.serial = serial.FileSerial(file)
            return
        port = core.config['serial_port']
        if not port and core.debug:
            core.logger.debug(
                'In debug mode connect to fakeSerial, for force port add '
                '"serial_port" in config.yml')
            self.serial = serial.FakeSerial()
            return
        if not port:
            ports = serial.serial_ports_get()
            port = serial.serial_discovery_port(ports, quick=True)
        if not port:
            print(
                'You must set a "serial_port" value for config file '
                'config.yml')
            sys.exit(-1)
        core.logger.debug('Connect to port "%s"' % port)
        self.serial = serial.serial_get(port)
        if not self.serial.is_open:
            raise Exception('Can\'t open serial port %s' % port)
        self.serial.reset_input_buffer()

    def serial_send(self, msg):
        core.logger.info('Serial send "%s"' % msg)
        self.serial.write(bytes('%s\r\n' % msg, 'utf8'))
        self.serial.flush()

    def serial_read(self):
        threading.Timer(0.1, self.serial_read).start()
        core.logger.info('Reading')
        line = self.serial.read_until()
        if not line:
            return
        data = line.strip().decode().split(' ')
        core.logger.debug('Read line: %s' % data)
        # frame: CONFIG pip peep rpm
        if data[0] == 'CONFIG':
            if self._ignore_config:
                core.logger.debug('Ignore config')
                return
            if len(data) != 11:
                return

            self._config_pip = int(data[1])
            self._config_peep = int(data[2])
            self._config_fr = int(data[3])
            self._fr = int(data[3])
            self._config_ie = int(data[4])
            self._config_ie_vcv = int(data[4])
            self._config_threshold = int(data[5])

            mode = int(data[6])
            if mode == 1: # assisted mode
                self._trigger = True
                self._recruit = False
            elif mode == 2: # recruit mode
                self._trigger = False
                self._recruit = True
            else: # controlled mode
                self._trigger = False
                self._recruit = False

            control = int(data[7])
            #if control == 0: # pressure control
            #    self.tabWidget.setCurrentIndex(0)
            #elif control == 1: # volume control
            #    self.tabWidget.setCurrentIndex(1)

            #self._config_tidal_vol = int(data[8])
            #self._config_flow_pcv = int(data[9])
            #self._config_flow_vcv = int(data[9])
            #self._running = bool(int(data[10]))
            timestamp = time.time()
            msg = "reespirator.%d.config.pip %d %d\r\n" % (core.config['id'], data[1], timestamp)
            msg += "reespirator.%d.config.peep %d %d\r\n" % (core.config['id'], data[2], timestamp)
            core.logger.debug(msg)

        # frame: DT pres1 pres2 vol flow
        elif data[0] == 'DT':
            self._pres1 = int(data[1])
            self._pres2 = int(data[3])
            # self._vol = int(data[3])
            self._flow = int(data[4])
            self.update()
        # frame: VOL vol
        elif data[0] == 'VOL':
            # self._vol = int(data[1])
            # self.update()
            pass
        # frame: EOC pip peep vol
        elif data[0] == 'EOC':
            self._pip = int(data[1])
            self._peep = int(data[2])
            self._vol = int(data[3])
            # TODO: Add CO2 to comms protocol
            self._co2 = 0.0
            # self._co2 = int(data[5])
            self.update()



def app():
    main = MainBridge()
# vim: tabstop=4 ai expandtab shiftwidth=4 softtabstop=4
