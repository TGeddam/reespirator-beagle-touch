##############################################################################
# For copyright and license notices, see LICENSE file in root directory
##############################################################################
import sys
import socket
import numpy as np
import pyqtgraph as pg
from PyQt5 import QtCore, QtGui, QtWidgets, uic

from respyrator import core, serial

pg.setConfigOption('background', '052049')


class MainWindow(QtWidgets.QDialog):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._ignore_config = []
        self._serial = None
        self._pres1 = 0
        self._pres2 = 0
        self._pip = 20
        self._peep = 6
        self._fr = 14
        self._flow = 0
        self._vol = 99
        self._volume = 0

        self._running = False

        self._config_pip = 20
        self._config_peep = 6
        self._config_fr = 14
        self._config_ie = 33
        self._config_flow_pcv = 30

        self._config_tidal_vol = 240
        self._config_flow_vcv = 30
        self._config_peep_vcv = 10
        self._config_fr_vcv = 14
        self._config_ie_vcv = 33

        self._config_threshold = 3

        self._recruit = False
        self._recruit_on_text = 'STOP RECRUIT'
        self._recruit_off_text = 'START RECRUIT'
        self._recruit_timmer = None

        self._trigger = False
        self._trigger_on_text = 'STOP TRIGGER'
        self._trigger_off_text = 'START TRIGGER'

        self._alarm = 0
        self._alarm_is_muted = True

        self._s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        core.logger.debug('Trying to connect with %s %d' % (core.config['host'] , core.config['remotePort']))
        self._s.connect((core.config['host'], core.config['remotePort']))

        self.serial_setup()
        uic.loadUi(core.path('ui_main.ui'), self)

        # Attach events
        # Up
        self.buttonUpPip.clicked.connect(self.buttonUpPipClicked)
        self.buttonUpPeep.clicked.connect(self.buttonUpPeepClicked)
        self.buttonUpPeepVCV.clicked.connect(self.buttonUpPeepVCVClicked)
        self.buttonUpFR.clicked.connect(self.buttonUpFRClicked)
        self.buttonUpFRVCV.clicked.connect(self.buttonUpFRVCVClicked)
        self.buttonUpIE.clicked.connect(self.buttonUpIEClicked)
        self.buttonUpIEVCV.clicked.connect(self.buttonUpIEVCVClicked)
        self.buttonUpTriggerThreshold.clicked.connect(self.buttonUpTriggerThresholdClicked)
        self.buttonUpFlowPCV.clicked.connect(self.buttonUpFlowPCVClicked)
        self.buttonUpFlowVCV.clicked.connect(self.buttonUpFlowVCVClicked)
        self.buttonUpTidalVol.clicked.connect(self.buttonUpTidalVolClicked)

        # Down
        self.buttonDownPip.clicked.connect(self.buttonDownPipClicked)
        self.buttonDownPeep.clicked.connect(self.buttonDownPeepClicked)
        self.buttonDownPeepVCV.clicked.connect(self.buttonDownPeepVCVClicked)
        self.buttonDownFR.clicked.connect(self.buttonDownFRClicked)
        self.buttonDownFRVCV.clicked.connect(self.buttonDownFRVCVClicked)
        self.buttonDownIE.clicked.connect(self.buttonDownIEClicked)
        self.buttonDownIEVCV.clicked.connect(self.buttonDownIEVCVClicked)
        self.buttonDownTriggerThreshold.clicked.connect(self.buttonDownTriggerThresholdClicked)
        self.buttonDownFlowPCV.clicked.connect(self.buttonDownFlowPCVClicked)
        self.buttonDownFlowVCV.clicked.connect(self.buttonDownFlowVCVClicked)
        self.buttonDownTidalVol.clicked.connect(self.buttonDownTidalVolClicked)

        # Layout
        self.buttonStartStopCollapser.clicked.connect(self.buttonStartStopCollapserClicked)
        self.buttonRecruitCollapser.clicked.connect(self.buttonRecruitCollapserClicked)
        self.buttonTriggerCollapser.clicked.connect(self.buttonTriggerCollapserClicked)
        self.buttonConfigMode.clicked.connect(self.buttonConfigModeClicked)

        # Commands
        self.buttonRecruit.clicked.connect(self.buttonRecruitClicked)
        self.buttonTrigger.clicked.connect(self.buttonTriggerClicked)
        self.buttonMuteAlarm.clicked.connect(self.buttonMuteAlarmClicked)
        self.buttonStartStop.clicked.connect(self.buttonStartStopClicked)
        self.tabWidget.currentChanged.connect(self.tabWidgetChanged)

        # Layouts
        self.boxRecruit.setVisible(False)
        self.boxStartStop.setVisible(False)
        self.boxTrigger.setVisible(False)
        self.boxAlarm.setVisible(False)
        self.stackedWidget.setCurrentIndex(0)

        self.buttonConfigMode.setStyleSheet('background-color: #506380')
        self._command_on_stylesheet = 'background-color: #c00'
        self._command_off_stylesheet = 'background-color: #506380'

        # Timers
        self.timer = pg.QtCore.QTimer()
        self.timer.timeout.connect(self.serial_read)

        # Graphs
        self.myCurve = [0, 0, 0]
        self.chunkSize = 200
        self.split = 100
        self.xAxis = np.arange(self.chunkSize)
        self.data1 = np.zeros((self.chunkSize, 3))
        self.plot(0, self.graphPressure, 'P', self.xAxis, self.data1[:, 0], 'f48024')
        self.plot(1, self.graphFlow,     'C', self.xAxis, self.data1[:, 1], '18a3ac')
        self.plot(2, self.graphVolume,   'V', self.xAxis, self.data1[:, 2], '178ccb')
        self.pointer = 0
        self.firstCycle = 1

        self.update()

    def show(self, *args, **kwargs):
        res = super().show()
        self.timer.start(10)
        return res

    def serial_setup(self):
        file = core.config['serial_file']
        if file:
            self.serial = serial.FileSerial(file)
            return
        port = core.config['serial_port']
        if not port and core.debug:
            core.logger.debug(
                'In debug mode connect to fakeSerial, for force port add '
                '"serial_port" in config.yml')
            self.serial = serial.FakeSerial()
            return
        if not port:
            ports = serial.serial_ports_get()
            port = serial.serial_discovery_port(ports, quick=True)
        if not port:
            print(
                'You must set a "serial_port" value for config file '
                'config.yml')
            sys.exit(-1)
        core.logger.debug('Connect to port "%s"' % port)
        self.serial = serial.serial_get(port)
        if not self.serial.is_open:
            raise Exception('Can\'t open serial port %s' % port)
        self.serial.reset_input_buffer()

    def serial_send(self, msg):
        def no_ignore_config():
            if self._ignore_config:
                self._ignore_config.pop(0)

        self._ignore_config.append(1)
        pg.QtCore.QTimer().singleShot(1000, no_ignore_config)

        timer = pg.QtCore.QTimer()
        timer.timeout.connect(self.serial_read)

        core.logger.info('Serial send "%s"' % msg)
        self.serial.write(bytes('%s\r\n' % msg, 'utf8'))
        self.serial.flush()

    def serial_read(self):
        line = self.serial.read_until()
        if not line:
            return
        self._s.send((core.config['id'] + " ").encode() + line)
        try:
            data = line.strip().decode().split(' ')
        except UnicodeDecodeError:
            core.logger.debug('Error decoding incoming data', exc_info=True)
            return
        core.logger.debug('Read line: %s' % data)
        # frame: CONFIG pip peep rpm
        if data[0] == 'CONFIG':
            if self._ignore_config:
                core.logger.debug('Ignore config')
                return
            if len(data) != 11:
                return

            self._config_pip = int(data[1])
            self._config_peep = int(data[2])
            self._config_fr = int(data[3])
            self._fr = int(data[3])
            self._config_ie = int(data[4])
            self._config_ie_vcv = int(data[4])
            self._config_threshold = int(data[5])

            mode = int(data[6])
            if mode == 1: # assisted mode
                self._trigger = True
                self._recruit = False
            elif mode == 2: # recruit mode
                self._trigger = False
                self._recruit = True
            else: # controlled mode
                self._trigger = False
                self._recruit = False

            control = int(data[7])
            if control == 0: # pressure control
                self.tabWidget.setCurrentIndex(0)
            elif control == 1: # volume control
                self.tabWidget.setCurrentIndex(1)

            self._config_tidal_vol = int(data[8])
            self._config_flow_pcv = int(data[9])
            self._config_flow_vcv = int(data[9])
            self._running = bool(int(data[10]))

            # TODO: CHANGE APPEARANCE OF TRIGGER, RECRUIT AND STARTSTOP BUTTONS

            self.update()
        # frame: DT pres1 vol alarm
        elif data[0] == 'DT':
            self._pres1 = int(data[1])
            self._flow = int(data[2])
            self._volume = int(data[3])
            self._alarm = int(data[4])
            self.update()
        # frame: EOC pip peep vol
        elif data[0] == 'EOC':
            self._pip = int(data[1])
            self._peep = int(data[2])
            self._vol = int(data[3])
            # TODO: Add CO2 to comms protocol
            self._co2 = 0.0
            # self._co2 = int(data[5])
            self.update()
        elif data[0] == 'MUTEXP':
            self._alarm_is_muted = True
            self.buttonRecruit.setStyleSheet('background-color: #600;')
            self.buttonMuteAlarm.setText('MUTE')

    def plot(self, chartIndex, widget, title, hour, temperature, color):
        self.myCurve[chartIndex] = widget.plot(hour, temperature, title=title, fillLevel=0, brush=pg.mkBrush(color))
        self.myCurve[chartIndex].setPen(pg.mkPen(color, width=3))
        if chartIndex == 0:
            widget.setYRange(0, 50)
        elif chartIndex == 1:
            widget.setYRange(-50, 50)
        elif chartIndex == 2:
            widget.setYRange(-350, 350)
        widget.disableAutoRange()
        #widget.setXRange(0, self.chunkSize, padding=0)
        widget.showGrid(True, True, 1)
        widget.getPlotItem().hideAxis('bottom')
        widget.getPlotItem().setMouseEnabled(x=False, y=False)
        # widget.getPlotItem().hideAxis('left')

    @property
    def _compliance(self) -> float:
        try:
            return self._vol / (self._pip - self._peep)
        except ZeroDivisionError:
            return 0

    # @property
    # def _co2mmHg(self) -> float:
    #     """ Convert CO2 ppm to mmHg """
    #     cmh2O_to_mmHg = 0.735559
    #     return self._co2 * 1e-6 * self._pres1 * cmh2O_to_mmHg

    def update(self):
        # PCV
        self.configPip.setText(str(self._config_pip))
        self.configPeep.setText(str(self._config_peep))
        self.configFr.setText(str(self._config_fr))
        self.configIE.setText(str(self._config_ie))
        self.configFlowPCV.setText(str(self._config_flow_pcv))
        # VCV
        self.configTidalVol.setText(str(self._config_tidal_vol))
        self.configFlowVCV.setText(str(self._config_flow_vcv))
        self.configPeepVCV.setText(str(self._config_peep_vcv))
        self.configFRVCV.setText(str(self._config_fr_vcv))
        self.configIEVCV.setText(str(self._config_ie_vcv))
        # Trigger
        self.configTriggerThreshold.setText(str(self._config_threshold))
        # Real-time data
        self.textPip.setText(str(self._pip))
        self.textPeep.setText(str(self._peep))
        self.textFr.setText(str(self._fr))
        self.textVol.setText(str(self._vol))
        self.textComp.setText(str(round(self._compliance, 1)))
        # self.textCO2.setText(str(round(self._co2mmHg, 0)))

        # Alarms
        self.update_alarms()

        # StartStop
        self.update_startstop()

        pres = self._pres1 if self._pres1 else 0
        flow = self._flow / 1000.0 if self._flow else 0
        volume = self._volume if self._running else 0
        self.i = self.pointer % (self.chunkSize)
        if self.i == 0 and self.firstCycle == 0:
            tmp = np.empty((self.chunkSize, 3))
            tmp[:self.split] = self.data1[self.chunkSize - self.split:]
            self.data1 = tmp
            self.pointer = self.split
            self.i = self.pointer
        self.data1[self.i, 0] = pres
        self.data1[self.i, 1] = flow
        self.data1[self.i, 2] = volume
        self.myCurve[0].setData(
            x=self.xAxis[:self.i + 1],
            y=self.data1[:self.i + 1, 0],
            clear=True
        )
        self.myCurve[1].setData(
            x=self.xAxis[:self.i + 1],
            y=self.data1[:self.i + 1, 1],
            clear=True
        )
        self.myCurve[2].setData(
            x=self.xAxis[:self.i + 1],
            y=self.data1[:self.i + 1, 2],
            clear=True
        )
        QtGui.QApplication.processEvents()
        self.pointer += 1
        if self.pointer >= self.chunkSize:
            self.firstCycle = 0

    def update_startstop(self):
        if self._running:
            self.buttonStartStopCollapser.setText('STOP')
        else:
            self.buttonStartStopCollapser.setText('START')

    def update_alarms(self):
        alarm = self._alarm
        if alarm == 0:
            self.boxAlarm.setVisible(False)
            return

        self.boxAlarm.setVisible(True)
        if alarm == 10:
            self.textAlarmTitle.setText('Overpressure')
        elif alarm == 11:
            self.textAlarmTitle.setText('High pressure')
        elif alarm == 12:
            self.textAlarmTitle.setText('No pressure')
        elif alarm == 20:
            self.textAlarmTitle.setText('No flow')

    def kill_recruit_timmer(self):
        if not self._recruit_timmer:
            return
        self._recruit_timmer.stop()
        self._recruit_timmer.deleteLater()
        self._recruit_timmer = None

    def start_recruit_timmer(self):
        self._recruit_timmer = QtCore.QTimer()
        self._recruit_timmer.timeout.connect(self.recruit_off)
        self._recruit_timmer.setSingleShot(True)
        self._recruit_timmer.start(40000)


    def trigger_on(self):
        self.serial_send('TRIGGER 1')
        self._trigger = True
        self.buttonTrigger.setStyleSheet(self._command_on_stylesheet)
        self.buttonTrigger.setText(self._trigger_on_text)
        self.buttonTriggerCollapser.setStyleSheet('background-color: #178CCB;')
        self.start_recruit_timmer()

    def trigger_off(self):
        self.serial_send('TRIGGER 0')
        self._trigger = False
        self.buttonTriggerCollapser.setStyleSheet(self._command_off_stylesheet)
        self.buttonTrigger.setStyleSheet(self._command_off_stylesheet)
        self.buttonTrigger.setText(self._trigger_off_text)


    def recruit_on(self):
        self.kill_recruit_timmer()
        self.serial_send('RECRUIT 1')
        self._recruit = True
        self.buttonRecruit.setStyleSheet(self._command_on_stylesheet)
        self.buttonRecruit.setText(self._recruit_on_text)
        self.buttonRecruitCollapser.setStyleSheet('background-color: #178CCB;')
        self.start_recruit_timmer()

    def recruit_off(self):
        self.kill_recruit_timmer()
        self.serial_send('RECRUIT 0')
        self._recruit = False
        self.buttonRecruit.setStyleSheet(self._command_off_stylesheet)
        self.buttonRecruit.setText(self._recruit_off_text)
        self.buttonRecruitCollapser.setStyleSheet(self._command_off_stylesheet)
        self.boxRecruit.setVisible(False)


    def alarm_on(self):
        # TODO: kill alarm trigger
        self.serial_send('MUTE 1')
        self._alarm_is_muted = False
        self.buttonRecruit.setStyleSheet('background-color: #900;')
        self.buttonMuteAlarm.setText('UNMUTE')

    def alarm_off(self):
        # TODO: kill alarm trigger
        self.serial_send('MUTE 0')
        self._alarm_is_muted = True
        self.buttonRecruit.setStyleSheet('background-color: #600;')
        self.buttonMuteAlarm.setText('MUTE')


    def start_ventilation(self):
        self.serial_send('START')
        self._running = True
        self.buttonStartStopCollapser.setText('STOP')
        self.buttonStartStopCollapser.setStyleSheet(self._command_off_stylesheet)
        self.boxStartStop.setVisible(False)

    def stop_ventilation(self):
        self.serial_send('STOP')
        self._running = False
        self.buttonStartStopCollapser.setText('START')
        self.buttonStartStopCollapser.setStyleSheet(self._command_off_stylesheet)
        self.boxStartStop.setVisible(False)


    def buttonUpTidalVolClicked(self):
        if self._config_tidal_vol <= 1000:
            self._config_tidal_vol += 10
        self.update()
        self.serial_send('CONFIG VT %s' % self._config_tidal_vol)

    def buttonDownTidalVolClicked(self):
        if self._config_tidal_vol >= 50:
            self._config_tidal_vol -= 10
        self.update()
        self.serial_send('CONFIG VT %s' % self._config_tidal_vol)


    def buttonUpPipClicked(self):
        if self._config_pip <= 79:
            self._config_pip += 1
        self.update()
        self.serial_send('CONFIG PIP %s' % self._config_pip)

    def buttonDownPipClicked(self):
        if self._config_pip >= 1:
            self._config_pip -= 1
        self.update()
        self.serial_send('CONFIG PIP %s' % self._config_pip)


    def buttonUpPeepClicked(self):
        if self._config_peep < self._config_pip:
            self._config_peep += 1
        self.update()
        self.serial_send('CONFIG PEEP %s' % self._config_peep)

    def buttonDownPeepClicked(self):
        if self._config_peep > 0:
            self._config_peep -= 1
        self.update()
        self.serial_send('CONFIG PEEP %s' % self._config_peep)


    def buttonUpPeepVCVClicked(self):
        if self._config_peep_vcv < 60:
            self._config_peep_vcv += 1
        self.update()
        self.serial_send('CONFIG PEEP %s' % self._config_peep_vcv)

    def buttonDownPeepVCVClicked(self):
        if self._config_peep_vcv > 0:
            self._config_peep_vcv -= 1
        self.update()
        self.serial_send('CONFIG PEEP %s' % self._config_peep_vcv)


    def buttonUpFRClicked(self):
        if self._config_fr < 30:
            self._config_fr += 1
        self.update()
        self.serial_send('CONFIG BPM %s' % self._config_fr)

    def buttonDownFRClicked(self):
        if self._config_fr > 3:
            self._config_fr -= 1
        self.update()
        self.serial_send('CONFIG BPM %s' % self._config_fr)


    def buttonUpFRVCVClicked(self):
        if self._config_fr_vcv < 30:
            self._config_fr_vcv += 1
        self.update()
        self.serial_send('CONFIG BPM %s' % self._config_fr_vcv)

    def buttonDownFRVCVClicked(self):
        if self._config_fr_vcv > 3:
            self._config_fr_vcv -= 1
        self.update()
        self.serial_send('CONFIG BPM %s' % self._config_fr_vcv)


    def buttonUpIEClicked(self):
        if self._config_ie < 50:
            self._config_ie += 1
        self.update()
        self.serial_send('CONFIG IE %s' % self._config_ie)

    def buttonDownIEClicked(self):
        if self._config_ie > 10:
            self._config_ie -= 1
        self.update()
        self.serial_send('CONFIG IE %s' % self._config_ie)


    def buttonUpIEVCVClicked(self):
        if self._config_ie_vcv < 50:
            self._config_ie_vcv += 1
        self.update()
        self.serial_send('CONFIG IE %s' % self._config_ie_vcv)

    def buttonDownIEVCVClicked(self):
        if self._config_ie_vcv > 10:
            self._config_ie_vcv -= 1
        self.update()
        self.serial_send('CONFIG IE %s' % self._config_ie_vcv)


    def buttonUpFlowPCVClicked(self):
        if self._config_flow_pcv < 90:
            self._config_flow_pcv += 1
        self.update()
        self.serial_send('CONFIG FLOW %s' % self._config_flow_pcv)

    def buttonDownFlowPCVClicked(self):
        if self._config_flow_pcv > 0:
            self._config_flow_pcv -= 1
        self.update()
        self.serial_send('CONFIG FLOW %s' % self._config_flow_pcv)


    def buttonUpFlowVCVClicked(self):
        if self._config_flow_vcv < 90:
            self._config_flow_vcv += 1
        self.update()
        self.serial_send('CONFIG FLOW %s' % self._config_flow_vcv)

    def buttonDownFlowVCVClicked(self):
        if self._config_flow_vcv > 0:
            self._config_flow_vcv -= 1
        self.update()
        self.serial_send('CONFIG FLOW %s' % self._config_flow_vcv)


    def buttonUpPipClicked(self):
        if self._config_pip <= 79:
            self._config_pip += 1
        self.update()
        self.serial_send('CONFIG PIP %s' % self._config_pip)

    def buttonDownPipClicked(self):
        if self._config_pip >= 1:
            self._config_pip -= 1
        self.update()
        self.serial_send('CONFIG PIP %s' % self._config_pip)


    def buttonUpTriggerThresholdClicked(self):
        if self._config_threshold < 30:
            self._config_threshold += 1
        self.update()
        self.serial_send('CONFIG TRIGTH %s' % self._config_threshold)

    def buttonDownTriggerThresholdClicked(self):
        if self._config_threshold > 0:
            self._config_threshold -= 1
        self.update()
        self.serial_send('CONFIG TRIGTH %s' % self._config_threshold)


    def buttonRecruitClicked(self):
        if self._recruit:
            self.recruit_off()
        else:
            self.recruit_on()


    def buttonTriggerClicked(self):
        if self._trigger:
            self.trigger_off()
        else:
            self.trigger_on()


    def buttonMuteAlarmClicked(self):
        if self._alarm_is_muted:
            self.alarm_on()
        else:
            self.alarm_off()

    def buttonStartStopClicked(self):
        if self._running:
            self.stop_ventilation()
        else:
            self.start_ventilation()


    def buttonConfigModeClicked(self):
        if self.buttonConfigMode.isChecked():
            self.stackedWidget.setCurrentIndex(1)
            self.buttonConfigMode.setStyleSheet("background-color: #18a3ac")
        else:
            self.stackedWidget.setCurrentIndex(0)
            self.buttonConfigMode.setStyleSheet("background-color: #506380")

    def buttonTriggerCollapserClicked(self):
        if self.buttonTriggerCollapser.isChecked():
            self.boxTrigger.setVisible(False)
            self.buttonTriggerCollapser.setStyleSheet("background-color: #506380")
        else:
            self.boxTrigger.setVisible(True)
            self.buttonTriggerCollapser.setStyleSheet("background-color: #18a3ac")

    def buttonRecruitCollapserClicked(self):
        # Override behavior if recruit is active
        if self._recruit:
            self.boxRecruit.setVisible(True)
            return

        if self.buttonRecruitCollapser.isChecked():
            self.boxRecruit.setVisible(False)
            self.buttonRecruitCollapser.setStyleSheet("background-color: #506380")
        else:
            self.boxRecruit.setVisible(True)
            self.buttonRecruitCollapser.setStyleSheet("background-color: #18a3ac")

    def buttonStartStopCollapserClicked(self):
        if self.buttonStartStopCollapser.isChecked():
            self.boxStartStop.setVisible(False)
            self.buttonStartStopCollapser.setStyleSheet("background-color: #506380")
        else:
            self.boxStartStop.setVisible(True)
            self.buttonStartStopCollapser.setStyleSheet("background-color: #18a3ac")

    def tabWidgetChanged(self, i: int):
        if i == 0:
            self.serial_send('PCV')
        elif i == 1:
            self.serial_send('VCV')

def app():
    app = QtWidgets.QApplication(sys.argv)
    main = MainWindow()
    main.show()
    sys.exit(app.exec_())
