#!/bin/bash
/bin/echo 49 > /sys/class/gpio/export
#set for output
/bin/echo "out" > /sys/class/gpio/gpio49/direction
#set HIGH
/bin/echo 1 > /sys/class/gpio/gpio49/value
/usr/bin/X &
/bin/sleep 3
export DISPLAY=:0
cd /root/repos/reespirator-beagle-touch/src/
killall -9 /usr/bin/python3; /usr/bin/nice -n -10 /usr/bin/python3 respyratorctl ui
